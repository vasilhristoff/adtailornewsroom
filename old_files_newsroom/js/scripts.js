$(document).ready(function() {
	
// video 
	$(".video-play").on("click", function() {
		$(".overlay").show();
		$("#video").each(function () { this.play() });
	});
	
	$(".close-video").on("click", function() {
		$(".overlay").hide();
		$("#video").each(function () { this.pause() });
	});
// end video 

// insert smarttlook
	$("body").prepend('<script>window.smartlook||function(t){var e=smartlook=function(){e.api.push(arguments)},a=t.getElementsByTagName("script")[0],r=t.createElement("script");e.api=new Array,r.async=!0,r.type="text/javascript",r.charset="utf-8",r.src="//rec.getsmartlook.com/bundle.js",a.parentNode.insertBefore(r,a)}(document),smartlook("init","47685972971c5574d140c7a42f6740c5a8f00882");</script>');

$(".events-calendar-headings h3").on("click" , function() {
   $(".events-calendar-headings h3").removeClass("active");
   $(".events-calendar-col").hide();
   $(this).addClass("active");
   if($(this).hasClass("events-calendar-upcomming-heading")) {
   		$(".upcomming-events").show();
   } else {
   		$(".past-events").show();
   }
});

// google analytics
	
	// ---- google analytics tracking code
//	$("body").prepend("<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga'); ga('create', 'UA-80444514-1', 'auto'); ga('send', 'pageview');</script>");
	// event racking scripts
/*	$(".news-post-featured-image").find("a").on('click', function () {
		var postHref = $(this).attr("href");
		var postHeading = $(this).parent().parent().find("h3").text();
		ga('send','event', '' + postHeading + ' Link','Click','' + postHref + '');
	});

	$(".media-folder-thumbnail").find("a").on("click" , function() {
		var mediaFolderRef = $(this).attr("href");
		var mediaFolderName = $(this).parent().parent().find("h4").text();
		ga('send','event', '' + mediaFolderName + ' link','Click','' + mediaFolderRef + '');
	});*/
// end google analytics
}); 

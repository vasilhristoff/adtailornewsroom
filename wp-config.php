<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */



// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'AdtailorWordpress');

/** MySQL database username */
define('DB_USER', 'wordpressuser');

/** MySQL database password */
define('DB_PASSWORD', 'roliatdadb846250');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/* enhance the filezize upload  */
define('WP_MEMORY_LIMIT', '64M');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '1n}%{:S_7#b,{gd}(2LJcU@yao}{PQDyeAYz+o9O`RPUGA=BVy.(L,1E/e78Jm!j');
define('SECURE_AUTH_KEY',  '~Gd_fJ]-x3V$T2 <ASE,[ ~O+)jsNIsLV4E*F^rt!o?ePV:1Upl/?q].-p,`|EQM');
define('LOGGED_IN_KEY',    ' v2DE4^LRw!I!52zs|tyw)RwcA>*d--tE7KyLt60Sxi{$RL6}-T_?G2>DL?d*Lo;');
define('NONCE_KEY',        'RzxL[`g~wrB:]3ajA>[n@^fYuJ5(p]}3A6_[hA3kKF4Vo`p<);h]MWY~o6}IU:l1');
define('AUTH_SALT',        'arb,0zI}`]$MAL.TvjKg>Hz<;Carz.Z1$f. 2Mx(cPaTtL,q>,39TC AMo<(X`Tt');
define('SECURE_AUTH_SALT', 'IyLRB#U5DL>.eW<P]GG7x*i&s)pL#<i00h;Z$t?-A|E.y-=7S<>Imw,1$qrhgPXo');
define('LOGGED_IN_SALT',   'q -jIv/k:tZ*)8lEed;#{V-k@5~)EY1_1K@#VlYmxy2`pm7[aR$:*IxiX8dhb,{g');
define('NONCE_SALT',       '@Oykc16l7!7pVV5t@n+nW7bcNh4%}qzKi)8SK*S3N=_Y__fWXL.W=FI(95<&JE56');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

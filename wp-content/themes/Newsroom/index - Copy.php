<?php get_header(); ?>

<div class="main-content container">
		<div class="slider">
			<div class="slider-post-img">
				<img class="img-responsive" src="<?php bloginfo('stylesheet_directory'); ?>/img/affiliate-summit.jpg" alt="">
			</div>
			<div class="slider-post-content row">
				<h2 class="slider-post-heading col-md-10">
					<a href="">Beware, TC Disrupt, Adtailor is coming!</a>
				</h2>
				<span class="slider-post-date col-md-2 text-right">May 22 , 2020</span>
			</div>
			<div class="top-stories-tag">
				<span>Top News</span>
			</div>
		</div> <!-- end slider -->
		
		<div class="bottom-section row">
			<div class="bottom-section-left col-md-7">
				<h3>Recent News and Announcements</h3>
				<div class="featured-news">
					<div class="recent-post">
						<span class="recent-post-date">April 30, 2016</span>
						<h4><a href="">lorem</a></h4>
					</div>
					<div class="recent-post">
						<span class="recent-post-date">April 30, 2016</span>
						<h4><a href="">lorem</a></h4>
					</div>
					<div class="recent-post">
						<span class="recent-post-date">April 30, 2016</span>
						<h4><a href="">lorem</a></h4>
					</div>
					<div class="recent-post">
						<span class="recent-post-date">April 30, 2016</span>
						<h4><a href="">lorem</a></h4>
					</div>
					<div class="recent-post">
						<span class="recent-post-date">April 30, 2016</span>
						<h4><a href="">lorem</a></h4>
					</div>
			
				</div>
				<a href="news.html" class="link-news">see all</a>
			</div> <!--end bottom section left-->
			
			<div class="bottom-section-right col-md-5">
				<div class="contact-col">
					<h3>Contact us</h3>
					<a href="mailto:media@adtailor.com">media@adtailor.com</a>
				</div>
				
				<div class="events-calendar">
					<div class="events-calendar-headings row">
						<h3 class="col-md-6 upcomming-events-heading">Upcomming events</h3>
						<h3 class="col-md-6 past-events-heading">Past events</h3>
					</div>
					
					<div class="events-calendar-wrapper">
						<div class="upcomming-events">
							
							<div class="event">
								<p class="event-date">31 July - 2 August</p>
								<div class="event-description">
									<h4>Affiliate Summit East New York</h4>
									<p>Meet us at the Affiliate Summit in New York!</p>
								</div>
							</div>
							
							<div class="event">
								<p class="event-date">31 July - 2 August</p>
								<div class="event-description">
									<h4>Affiliate Summit East New York</h4>
									<p>Meet us at the Affiliate Summit in New York!</p>
								</div>
							</div>
							
							<div class="event">
								<p class="event-date">31 July - 2 August</p>
								<div class="event-description">
									<h4>Affiliate Summit East New York</h4>
									<p>Meet us at the Affiliate Summit in New York!</p>
								</div>
							</div>
							
							<div class="event">
								<p class="event-date">31 July - 2 August</p>
								<div class="event-description">
									<h4>Affiliate Summit East New York</h4>
									<p>Meet us at the Affiliate Summit in New York!</p>
								</div>
							</div>
							
							<div class="event">
								<p class="event-date">31 July - 2 August</p>
								<div class="event-description">
									<h4>Affiliate Summit East New York</h4>
									<p>Meet us at the Affiliate Summit in New York!</p>
								</div>
							</div>
							
						</div> 
					</div>
				</div> <!-- end calendar wrapper -->
				
				<div class="social-media-buttons">
					<h3>Social Media</h3>
					<ul>
						<li class="linkedin">
							<a href="https://www.linkedin.com/company/adtailor">Linkedin</a>
						</li>
						<li class="facebook">
							<a href="https://www.facebook.com/adtailor/?fref=ts">Facebook</a>
						</li>
						<li class="twitter">
							<a href="https://twitter.com/adtailor">Twitter</a>
						</li>
					</ul>
				</div>
			</div><!--end bottom section right-->
		</div>
	</div> <!-- end main content -->

<?php get_footer();?>
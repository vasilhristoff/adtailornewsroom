$(document).ready(function() {

	 // add hotjar heatmap
     $("body").append("<script>(function(h,o,t,j,a,r){h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};h._hjSettings={hjid:361091,hjsv:5};a=o.getElementsByTagName('head')[0];r=o.createElement('script');r.async=1;r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;a.appendChild(r);})(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');</script>");

	$(".video-play").on("click", function() {
		$(".overlay").show();
		$("#video").each(function () { this.play() });
	});
	
	$(".close-video").on("click", function() {
		$(".overlay").hide();
		$("#video").each(function () { this.pause() });
	});
	
	$(".events-calendar-headings h3").on("click" , function() { 
	   $(".events-calendar-headings h3").removeClass("active");
	   $(".events-calendar-headings h3").find(".glyphicon").remove();
	   $(".events-calendar-col").hide();
	   $(this).addClass("active");
	   if($(this).hasClass("upcomming-events-heading")) {
			$(".upcomming-events").show();
		    $(this).prepend('<span class="glyphicon glyphicon-triangle-right"></span>');
	   } else {
			$(".past-events").show();
		   $(".past-events-heading").prepend('<span class="glyphicon glyphicon-triangle-right"></span>');
	   }
	});
});

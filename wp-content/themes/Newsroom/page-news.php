<?php /* Template Name: Page News */ ?>
<?php remove_filter ('the_excerpt', 'wpautop'); ?>
<?php get_header(); ?>

<div class="main-content container">
		<div class="row nomargin col-post-wrapper">
			<?php 
				$args = array( 'category_name' => 'post_news,post_featured' );
				
				/*$myposts = get_posts( $args );*/
				$myposts = query_posts( $args );
				foreach ( $myposts as $post ) : setup_postdata( $post ); 
			?>
			<div class="wrapper col-md-6 col-post" id="<?php the_ID(); ?>">
				<span class="col-post-date"><span class="glyphicon glyphicon-calendar"></span><?php the_time('F jS, Y '); ?></span>
				<h3 style="padding-top:0 !important;"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
				<div class="col-post-img">
					<?php 
						if(has_post_thumbnail) {
							the_post_thumbnail(); 
						}
					?>
				</div>
				<p class="col-post-excerpt"><?php echo the_excerpt(); ?></p>
				<a href="<?php the_permalink(); ?>" class="col-post-link">read more<span class="glyphicon glyphicon-chevron-right"></span></a>
			</div>
			<?php endforeach;
					wp_reset_postdata(); ?>	
		</div>	
		
</div>	

<?php get_footer();?>











